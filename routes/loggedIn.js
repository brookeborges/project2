var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    var data = {};
    data.first = req.session.account.first;
    res.render('loggedIn.ejs',data)
});
router.get('/about', function(req,res){
    res.render('../views/aboutPage.ejs');
});
module.exports = router;