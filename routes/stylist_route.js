var express = require('express');
var router = express.Router();
var stylistDal = require('../model/stylist_dal');


router.get('/new', function(req, res) {
    res.render('stylist_insert_form');
});

router.get('/stylist_insert', function(req, res){
    stylistDal.Insert(req.query.first, req.query.last, req.query.email, req.query.password, function(err, result){
        var response = {};
        if(err) {
            response.message = err.message;
        }
        else {
            response.message = 'Success!';
        }
        res.json(response);
    });
});

router.get('/about', function(req,res){
    res.render('../views/aboutPage.ejs');
});
module.exports = router;