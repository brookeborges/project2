var express = require('express');
var router = express.Router();
var clientDal = require('../model/client_dal');


router.get('/new', function(req, res) {
    res.render('client_insert_form');
});

router.get('/client_insert', function(req, res){
    console.log("test");
    clientDal.Insert(req.query.first, req.query.last, req.query.email, req.query.client_phone, req.query.description, function(err, result){
        var response = {};
        if(err) {
            response.message = err.message;
        }
        else {
            response.message = 'Success!';
        }
        res.json(response);
    });
});


router.get('/about', function(req,res){
    res.render('../views/aboutPage.ejs');
});

router.get('/', function (req, res) {
    clientDal.GetByEmail(req.query.email, function (err, result) {
            if (err) throw err;

            res.render('displayClientInfo.ejs', {rs: result, email: req.query.email});
        }
    );
});

router.get('/all', function(req, res) {
    clientDal.GetAll(function (err, result) {
            if (err) throw err;
            res.render('displayAllClients.ejs', {rs: result});
        }
    );
});

router.post('/update_client', function(req,res){
    console.log(req.body);
    // first update the user
    clientDal.update(req.body.user_id, req.body.first, req.body.last, re.body.email, req.body.description,
        function(err){
            var message;
            if(err) {
                console.log(err);
                message = 'error: ' + err.message;
            }
            else {
                message = 'success';
            }
            // next update the states
            userdal.getbyid(req.body.client_id, function(err, user_info){
                res.redirect('/user/edit?client_id=' + req.body.user_id + '&message=' + message);
                //res.render('user/user_edit_form', {rs: user_info, states: state_result});
            });
        });
});

router.get('/delete', function(req, res){
    console.log(req.query);
    clientDal.GetByID(req.query.user_id, function(err, result) {
        if(err){
            res.send("error: " + err);
        }
        else if(result.length != 0) {
            clientdal.deletebyid(req.query.user_id, function (err) {
                res.send(result[0].first + ' successfully deleted');
            });
        }
        else {
            res.send('client does not exist in the database.');
        }
    });
});

router.get('/edit', function(req, res){
    console.log('/edit client_id:' + req.query.client_id);

    clientDal.GetByID(req.query.client_id, function(err, user_result){
        if(err) {
            console.log(err);
            res.send('error: ' + err);
        }
        else {
            console.log(user_result);
        }
    });
});

exports.Update = function(first, last, email, description, callback) {
    console.log(first, last, email, description);
    var values = [first, last, email, description];

    connection.query('UPDATE client SET first = ?, last = ?, email = ?, description = ? WHERE client_id = ?', values,
        function(err, result){
            if(err) {
                console.log(this.sql);
                callback(err, null);
            }
        });
}
module.exports = router;