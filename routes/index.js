var express = require('express');
var router = express.Router();
var stylistDal = require('../model/stylist_dal');
var clientDal = require('../model/client_dal');

/* GET home page. */
router.get('/', function(req, res, next) {
  var data = {
    title : 'Express'
  }
  if(req.session.account === undefined) {
    res.render('index', data);
  }
  else {
    data.first = req.session.account.first;
    res.render('index', data);
  }
});

router.get('/authenticate', function(req, res) {
  stylistDal.GetByEmail(req.query.email, req.query.password, function (err, account) {
    if (err) {
      res.send(err);
    }
    else if (account == null) {
      res.send("Account not found.");
    }
    else {
      req.session.account = account;
      if(req.session.originalUrl = '/login'){
        req.session.originalUrl = '/loggedIn';
      }
res.redirect(req.session.originalUrl);
    }
  });
});

router.get('/login', function(req, res) {
  res.render('../views/login.ejs');
});

router.get('/logout', function(req, res) {
  req.session.destroy( function(err) {
    res.render('../views/logout.ejs');
  });
});

router.get('/about', function(req,res){
  res.render('../views/aboutPage.ejs');
});

router.get('/findClientInfo',function(req,res){
  var clientData = {}
  clientDal.GetByEmail(req.query.email, function(err,account){
    if(err) {
      res.send(err);
    }
    else if(account == null) {
      res.send("Account not found.");
    }
    else {
      clientData.first = account.first;
      clientData.last = account.last;
      clientData.description= account.description
      res.render('displayClientInfo', clientData);
    }
  });
});
module.exports = router;
