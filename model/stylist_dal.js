var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetByEmail = function(email, password, callback) {
    var query = "CALL Account_GetByEmail(?, ?)";
    var query_data = [email, password];

    connection.query(query, query_data, function(err, result) {
        if(err){
            callback(err, null);
        }
// NOTE: Stored Procedure results are wrapped in an extra array
 // and only one user record should be returned,
 // so return only the one result
 else if(result[0].length == 1) {
 callback(err, result[0][0]);
 }
 else {
 callback(err, null);
 }
 });
 }

exports.GetAll = function(callback){
    var qry = "SELECT * from stylist;"
    connection.query(qry, function(err, result){
        callback(err, result);
    });
}

exports.Insert = function(first, last, email, password, callback) {
    var qry = "INSERT INTO stylist (first, last, email, password) VALUES (?, ?, ?, ?)";
    connection.query(qry, [first, last, email, password], function(err, result){
        callback(err, result);
    });
}