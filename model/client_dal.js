var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.GetByEmail = function(email, callback) {
    var query = "CALL Client_GetByEmail(?)";
    var query_data = [email];

    connection.query(query, query_data, function(err, result) {
        if(err){
            callback(err, null);
        }
// NOTE: Stored Procedure results are wrapped in an extra array
        // and only one user record should be returned,
        // so return only the one result
        else if(result[0].length == 1) {
            callback(err, result[0][0]);
        }
        else {
            callback(err, null);
        }
    });
}
exports.GetAll = function(callback) {
    connection.query('SELECT * FROM client;',
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            console.log(result);
            callback(false, result);
        }
    );
}
exports.Insert = function(first, last, email, client_phone, description, callback) {
    var qry = "INSERT INTO client (first, last, email, client_phone, description) VALUES (?, ?, ?, ?, ?)";
    connection.query(qry, [first, last, email, client_phone, description], function(err, result){
        callback(err, result);
    });
}

exports.GetByID = function(client_id, callback) {
    console.log(client_id);
    var query = 'SELECT * FROM client WHERE client_id=' + client_id;
    console.log(query);
    connection.query(query,
        function (err, result) {
            if(err) {
                console.log(err);
                callback(true);
                return;
            }
            callback(false, result);
        }
    );
}
